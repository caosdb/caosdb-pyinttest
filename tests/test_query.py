# encoding: utf-8
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
"""Created on 26.01.2015.

@author: tf
"""


import os
import random

import caosdb as db
from caosdb.connection.connection import get_connection
from linkahead.utils.get_entity import get_entity_by_name, get_entity_by_id, get_entity_by_path
from caosdb.exceptions import EmptyUniqueQueryError, TransactionError
from lxml import etree
from pytest import mark, raises


def setup_module():
    db.administration.set_server_property("AUTH_OPTIONAL", "TRUE")
    perms = db.administration._get_permissions("anonymous")
    assert perms == set()


def setup_function(function):
    db.administration.set_server_property("FIND_QUERY_DEFAULT_ROLE", "ENTITY")
    try:
        db.execute_query("FIND ENTITY").delete()
    except Exception as e:
        print(e)


def teardown_function(function):
    db.configure_connection()
    setup_function(function)
    try:
        os.remove("test.dat")
    except Exception as e:
        print(e)


def teardown_module():
    db.administration.set_server_property("FIND_QUERY_DEFAULT_ROLE", "RECORD")


def test_query_with_reference_by_parent():
    db.RecordType("TestExperiment").insert()
    db.RecordType("TestProtocolLog").insert()
    db.RecordType("TestSpecialProtocolLog").add_parent(
        name="TestProtocolLog").insert()

    sp_rec = db.Record("TestSpecialProtocolLog1").add_parent(
        name="TestSpecialProtocolLog").insert()
    exp_rec = db.Record("TestExperiment1").add_parent(
        name="TestExperiment").add_property(
        name="TestProtocolLog",
        datatype=db.LIST("TestProtocolLog"),
        value=[
            sp_rec.id]).insert()
    assert 2 == len(db.execute_query("FIND TestExperiment"))
    assert exp_rec.id == db.execute_query(
        "FIND TestExperiment WHICH HAS A TestProtocolLog", unique=True).id
    assert exp_rec.id == db.execute_query(
        "FIND TestExperiment.TestProtocolLog", unique=True).id
    assert exp_rec.id == db.execute_query(
        "FIND TestExperiment WHICH REFERENCES " + str(sp_rec.id), unique=True).id
    assert exp_rec.id == db.execute_query(
        "FIND TestExperiment WHICH REFERENCES TestSpecialProtocolLog1",
        unique=True).id
    assert exp_rec.id == db.execute_query(
        "FIND TestExperiment WHICH REFERENCES TestSpecialProtocolLog",
        unique=True).id
    assert exp_rec.id == db.execute_query(
        "FIND TestExperiment .-> " + str(sp_rec.id), unique=True).id
    assert exp_rec.id == db.execute_query("FIND ENTITY " + str(exp_rec.id), unique=True).id
    assert exp_rec.id == db.execute_query("FIND RECORD " + str(exp_rec.id), unique=True).id
    assert exp_rec.id == db.execute_query(
        "FIND TestExperiment .-> TestSpecialProtocolLog1", unique=True).id
    assert exp_rec.id == db.execute_query(
        "FIND TestExperiment .-> TestSpecialProtocolLog", unique=True).id

    assert 0 == len(db.execute_query(
        "FIND TestExperiment WHICH HAS A TestProtocolLog1"))
    assert 0 == len(db.execute_query("FIND TestExperiment.TestProtocolLog1"))


def test_query_with_domains():
    person = db.RecordType("TestPerson").insert()
    db.Property("TestFirstName", datatype=db.TEXT).insert()
    db.Property("TestConductor", datatype=db.REFERENCE).insert()

    # TODO: new test for TestFirstName with overridden datatype=person
    dan = db.Record(
        name="TestDaniel").add_property(
        name="TestFirstName",
        value="Daniel").add_parent(person).insert()
    exp = db.RecordType(
        name="TestExperiment").add_property(
        name="TestConductor",
        datatype=person,
        value=dan.id).insert()

    assert db.execute_query(
        "FIND TestExperiment WHICH HAS A TestConductor->TestPerson",
        unique=True).id == exp.id
    assert db.execute_query(
        "FIND TestExperiment WHICH HAS A TestConductor=TestPerson",
        unique=True).id == exp.id
    assert db.execute_query(
        "FIND TestExperiment WHICH HAS A TestConductor=" + str(dan.id), unique=True).id == exp.id
    assert db.execute_query("FIND TestExperiment", unique=True).id == exp.id
    assert db.execute_query(
        "FIND TestExperiment WHICH HAS A TestConductor WHICH has a TestFirstName=Daniel",
        unique=True).id == exp.id


def test_query1():
    p = (
        db.Property(
            name="TestTextProperty",
            description="Test text property (from test_tenpoints.py)",
            datatype='text'))
    p.insert()

    assert int(p.id) >= 100
    assert p.id == db.execute_query("FIND TestTextProperty")[0].id
    assert p.id == db.execute_query("FIND TestTextProperty", unique=True).id


def test_query2():
    # create testfile
    f = open("test.dat", "w")
    f.write("hello world\n")
    f.close()

    ''' prepare file record '''
    f = db.File(name="TestFile", path='testfiles/testfile', file="test.dat")
    f.insert()

    f2 = db.File(name="TestFile2", path='testfiles/testfile2', file="test.dat")
    f2.insert()

    assert int(f.id) >= 100
    assert 2 == len(db.execute_query("FIND FILE"))
    assert f.id == db.execute_query(
        "FIND FILE WHICH IS STORED AT testfiles/testfile",
        unique=True).id
    assert f.id == db.File(path='testfiles/testfile').retrieve().id


def test_query3():
    body = get_connection().retrieve(
        entity_uri_segments=["Entity"], query_dict={
            "query": "bla"}, reconnect=True).read()
    print(body)
    xml = etree.fromstring(body)
    assert xml.xpath("/Response/TransactionBenchmark")
    assert xml.xpath("/Response/Query")
    assert xml.xpath("/Response/UserInfo")
    assert xml.xpath("/Response/noscript")
    assert xml.xpath("/Response/Error")
    assert 5 == len(xml)


@mark.slow
def test_conjunction():
    rt = db.RecordType(name="TestConjunctionTest").insert()
    assert rt.is_valid()

    pa = db.Property(
        name="TestConjunctionTestPropertyA",
        datatype="INTEGER").insert()
    assert pa.is_valid()

    pb = db.Property(
        name="TestConjunctionTestPropertyB",
        datatype="INTEGER").insert()
    assert pb.is_valid()

    ra = db.Record(
        name="TestA").add_parent(rt).add_property(
        property=pa,
        value="1").add_property(
            property=pb,
        value="0")
    rb = db.Record(
        name="TestB").add_parent(rt).add_property(
        property=pa,
        value="0").add_property(
            property=pb,
        value="1")
    rab = db.Record(
        name="TestAB").add_parent(rt).add_property(
        property=pa,
        value="1").add_property(
            property=pb,
        value="1")
    rn = db.Record(
        name="TestN").add_parent(rt).add_property(
        property=pa,
        value="0").add_property(
            property=pb,
        value="0")
    c = db.Container().extend([ra, rb, rab, rn]).insert()
    assert c.is_valid()

    assert 5 == len(db.execute_query("FIND TestConjunctionTest"))

    assert 4 == len(db.execute_query("FIND Record TestConjunctionTest"))

    assert 4 == len(db.execute_query(
        "FIND TestConjunctionTest . TestConjunctionTestPropertyA"))
    assert 4 == len(db.execute_query(
        "FIND TestConjunctionTest . TestConjunctionTestPropertyB"))
    assert 2 == len(db.execute_query(
        "FIND TestConjunctionTest . TestConjunctionTestPropertyB=1"))
    assert 2 == len(db.execute_query(
        "FIND TestConjunctionTest . TestConjunctionTestPropertyB=0"))
    assert 2 == len(db.execute_query(
        "FIND TestConjunctionTest . TestConjunctionTestPropertyA=1"))
    assert 2 == len(db.execute_query(
        "FIND TestConjunctionTest . TestConjunctionTestPropertyA=0"))

    assert 1 == len(db.execute_query(
        "FIND TestConjunctionTest . TestConjunctionTestPropertyA=1 AND TestConjunctionTestPropertyB=1"))
    assert 1 == len(db.execute_query(
        "FIND TestConjunctionTest . TestConjunctionTestPropertyA=0 AND TestConjunctionTestPropertyB=0"))
    assert 1 == len(db.execute_query(
        "FIND TestConjunctionTest . TestConjunctionTestPropertyA=0 AND TestConjunctionTestPropertyB=1"))
    assert 1 == len(db.execute_query(
        "FIND TestConjunctionTest . TestConjunctionTestPropertyA=1 AND TestConjunctionTestPropertyB=0"))

    assert 4 == len(db.execute_query(
        "FIND Record . TestConjunctionTestPropertyA"))
    assert 4 == len(db.execute_query(
        "FIND Record . TestConjunctionTestPropertyB"))
    assert 2 == len(db.execute_query(
        "FIND Record . TestConjunctionTestPropertyB=1"))
    assert 2 == len(db.execute_query(
        "FIND Record . TestConjunctionTestPropertyB=0"))
    assert 2 == len(db.execute_query(
        "FIND Record . TestConjunctionTestPropertyA=1"))
    assert 2 == len(db.execute_query(
        "FIND Record . TestConjunctionTestPropertyA=0"))

    assert 1 == len(db.execute_query(
        "FIND RECORD . TestConjunctionTestPropertyA=1 AND TestConjunctionTestPropertyB=1"))
    assert 1 == len(db.execute_query(
        "FIND RECORD . TestConjunctionTestPropertyA=0 AND TestConjunctionTestPropertyB=0"))
    assert 1 == len(db.execute_query(
        "FIND RECORD . TestConjunctionTestPropertyA=0 AND TestConjunctionTestPropertyB=1"))
    assert 1 == len(db.execute_query(
        "FIND RECORD . TestConjunctionTestPropertyA=1 AND TestConjunctionTestPropertyB=0"))


@mark.slow
def test_disjunction():
    rt = db.RecordType(name="TestDisjunctionTest").insert()
    assert rt.is_valid()

    pa = db.Property(
        name="TestDisjunctionTestPropertyA",
        datatype="INTEGER").insert()
    assert pa.is_valid()

    pb = db.Property(
        name="TestDisjunctionTestPropertyB",
        datatype="DOUBLE").insert()
    assert pb.is_valid()

    ra = db.Record(
        name="TestA").add_parent(rt).add_property(
        property=pa,
        value="1").add_property(
            property=pb,
        value="0")
    rb = db.Record(
        name="TestB").add_parent(rt).add_property(
        property=pa,
        value="0").add_property(
            property=pb,
        value="1")
    rab = db.Record(
        name="TestAB").add_parent(rt).add_property(
        property=pa,
        value="1").add_property(
            property=pb,
        value="1")
    rn = db.Record(
        name="TestN").add_parent(rt).add_property(
        property=pa,
        value="0").add_property(
            property=pb,
        value="0")
    c = db.Container().extend([ra, rb, rab, rn]).insert()
    assert c.is_valid()

    assert 5 == len(db.execute_query("FIND TestDisjunctionTest"))

    assert 4 == len(db.execute_query("FIND Record TestDisjunctionTest"))

    assert 4 == len(db.execute_query(
        "FIND TestDisjunctionTest . TestDisjunctionTestPropertyA"))
    assert 4 == len(db.execute_query(
        "FIND TestDisjunctionTest . TestDisjunctionTestPropertyB"))

    assert 2 == len(db.execute_query(
        "FIND TestDisjunctionTest . TestDisjunctionTestPropertyB=1"))
    assert 2 == len(db.execute_query(
        "FIND TestDisjunctionTest . TestDisjunctionTestPropertyB=0"))
    assert 2 == len(db.execute_query(
        "FIND TestDisjunctionTest . TestDisjunctionTestPropertyA=1"))
    assert 2 == len(db.execute_query(
        "FIND TestDisjunctionTest . TestDisjunctionTestPropertyA=0"))

    assert 3 == len(db.execute_query(
        "FIND TestDisjunctionTest . TestDisjunctionTestPropertyA=1 OR TestDisjunctionTestPropertyB=1"))
    assert 3 == len(db.execute_query(
        "FIND TestDisjunctionTest . TestDisjunctionTestPropertyA=0 OR TestDisjunctionTestPropertyB=0"))
    assert 3 == len(db.execute_query(
        "FIND TestDisjunctionTest . TestDisjunctionTestPropertyA=0 OR TestDisjunctionTestPropertyB=1"))
    assert 3 == len(db.execute_query(
        "FIND TestDisjunctionTest . TestDisjunctionTestPropertyA=1 OR TestDisjunctionTestPropertyB=0"))

    assert 2 == len(db.execute_query(
        "FIND TestDisjunctionTest . THE GREATEST TestDisjunctionTestPropertyB"))
    assert 3 == len(db.execute_query(
        "FIND TestDisjunctionTest . TestDisjunctionTestPropertyA=1 OR THE GREATEST TestDisjunctionTestPropertyB"))

    assert rn.id == db.execute_query(
        "FIND TestDisjunctionTest .  TestDisjunctionTestPropertyA='-1' OR ( TestDisjunctionTestPropertyB=0 AND TestDisjunctionTestPropertyA=0)",
        unique=True).id
    assert 3 == len(db.execute_query(
        "FIND TestDisjunctionTest . TestDisjunctionTestPropertyA=0 OR ( TestDisjunctionTestPropertyB=0 AND TestDisjunctionTestPropertyA=1)"))
    assert 2 == len(db.execute_query(
        "FIND TestDisjunctionTest . TestDisjunctionTestPropertyA=1 OR ( TestDisjunctionTestPropertyB=0 AND TestDisjunctionTestPropertyA=1)"))


@mark.slow
def test_greatest():
    pAB = db.Property(name="TestPropertyAB", datatype=db.DOUBLE).insert()
    assert pAB.is_valid()
    pA = db.Property(
        name="TestPropertyA",
        datatype=db.DOUBLE).add_parent(pAB).insert()
    assert pA.is_valid()
    pB = db.Property(name="TestPropertyB", datatype=db.DOUBLE).add_parent(pAB)
    assert len(pB.get_parents()) == 1
    print(pB)
    print(pB)
    pB.insert()
    assert pB.is_valid()

    rt = db.RecordType(name="TestRecordType").insert()
    assert rt.is_valid()

    rec1 = db.Record(name="TestRecord1").add_parent(
        rt).add_property(pA, value=1.0).insert()
    assert rec1.is_valid()

    c = db.execute_query(
        "FIND TestRecord1 WHICH HAS THE GREATEST TestPropertyA",
        unique=True)
    assert c.id == rec1.id

    c = db.execute_query(
        "FIND TestRecordType WHICH HAS THE GREATEST TestPropertyA",
        unique=True)
    assert c.id == rec1.id

    c = db.execute_query(
        "FIND TestRecord1 WHICH HAS THE GREATEST TestPropertyAB",
        unique=True)
    assert c.id == rec1.id

    c = db.execute_query(
        "FIND TestRecordType WHICH HAS THE GREATEST TestPropertyAB",
        unique=True)
    assert c.id == rec1.id

    rec2 = db.Record(name="TestRecord2").add_parent(
        rt).add_property(pA, value=2.0).insert()
    assert rec2.is_valid()

    c = db.execute_query(
        "FIND TestRecord2 WHICH HAS THE GREATEST TestPropertyA",
        unique=True)
    assert c.id == rec2.id

    c = db.execute_query(
        "FIND TestRecord1 WHICH HAS THE GREATEST TestPropertyA",
        unique=True)
    assert c.id == rec1.id

    c = db.execute_query(
        "FIND TestRecordType WHICH HAS THE GREATEST TestPropertyA",
        unique=True)
    assert c.id == rec2.id

    c = db.execute_query(
        "FIND TestRecord2 WHICH HAS THE GREATEST TestPropertyAB",
        unique=True)
    assert c.id == rec2.id

    c = db.execute_query(
        "FIND TestRecordType WHICH HAS THE GREATEST TestPropertyAB",
        unique=True)
    assert c.id == rec2.id

    rec3 = db.Record(name="TestRecord3").add_parent(
        rt).add_property(pA, value=1.5).insert()
    assert rec3.is_valid()

    c = db.execute_query(
        "FIND TestRecord3 WHICH HAS THE GREATEST TestPropertyA",
        unique=True)
    assert c.id == rec3.id

    c = db.execute_query(
        "FIND TestRecord2 WHICH HAS THE GREATEST TestPropertyA",
        unique=True)
    assert c.id == rec2.id

    c = db.execute_query(
        "FIND TestRecord1 WHICH HAS THE GREATEST TestPropertyA",
        unique=True)
    assert c.id == rec1.id

    c = db.execute_query(
        "FIND TestRecordType WHICH HAS THE GREATEST TestPropertyA",
        unique=True)
    assert c.id == rec2.id

    c = db.execute_query(
        "FIND TestRecord3 WHICH HAS THE GREATEST TestPropertyAB",
        unique=True)
    assert c.id == rec3.id

    c = db.execute_query(
        "FIND TestRecord2 WHICH HAS THE GREATEST TestPropertyAB",
        unique=True)
    assert c.id == rec2.id

    c = db.execute_query(
        "FIND TestRecord1 WHICH HAS THE GREATEST TestPropertyAB",
        unique=True)
    assert c.id == rec1.id

    c = db.execute_query(
        "FIND TestRecordType WHICH HAS THE GREATEST TestPropertyAB",
        unique=True)
    assert c.id == rec2.id

    rec4 = db.Record(name="TestRecord4").add_parent(
        rt).add_property(pB, value=0.1).insert()
    assert rec4.is_valid()

    c = db.execute_query(
        "FIND TestRecord4 WHICH HAS THE GREATEST TestPropertyA")
    assert len(c) == 0

    c = db.execute_query(
        "FIND TestRecord4 WHICH HAS THE GREATEST TestPropertyB",
        unique=True)
    assert c.id == rec4.id

    c = db.execute_query(
        "FIND TestRecordType WHICH HAS THE GREATEST TestPropertyB",
        unique=True)
    assert c.id == rec4.id

    c = db.execute_query(
        "FIND TestRecordType WHICH HAS THE GREATEST TestPropertyAB",
        unique=True)
    assert c.id == rec2.id

    rec5 = db.Record(
        name="TestRecord5").add_parent(rt).add_property(
        pB,
        value=200).add_property(
            pA,
        value=1.5).insert()
    assert rec5.is_valid()

    c = db.execute_query(
        "FIND TestRecordType WHICH HAS THE GREATEST TestPropertyAB",
        unique=True)
    assert c.id == rec5.id

    c = db.execute_query(
        "FIND TestRecordType WHICH HAS THE GREATEST TestPropertyB",
        unique=True)
    assert c.id == rec5.id

    c = db.execute_query(
        "FIND TestRecordType WHICH HAS THE GREATEST TestPropertyA",
        unique=True)
    assert c.id == rec2.id

    rec6 = db.Record(
        name="TestRecord6").add_parent(rt).add_property(
        pAB,
        value=400).add_property(
            pB,
            value=100).add_property(
                pA,
        value=2).insert()
    assert rec6.is_valid()

    c = db.execute_query(
        "FIND TestRecordType WHICH HAS THE GREATEST TestPropertyAB",
        unique=True)
    assert c.id == rec6.id

    c = db.execute_query(
        "FIND TestRecordType WHICH HAS THE GREATEST TestPropertyB",
        unique=True)
    assert c.id == rec5.id

    c = db.execute_query(
        "FIND TestRecordType WHICH HAS THE GREATEST TestPropertyA")
    assert c[0].id == rec2.id
    assert c[1].id == rec6.id


def test_wildcard_values():
    ptext = db.Property(name="TestTextProperty", datatype=db.TEXT).insert()
    rt = db.RecordType(
        name="TestRecordType").add_property(
        ptext, value="abcdefg").insert()

    assert rt.is_valid()
    rt2 = db.execute_query(
        'FIND TestRecordType WHICH HAS A TestTextProperty LIKE "abc*"',
        unique=True)
    assert rt2.id == rt.id

    rt2 = db.execute_query(
        'FIND TestRecordType WHICH HAS A TestTextProperty LIKE abc*',
        unique=True)
    assert rt2.id == rt.id

    rt2 = db.execute_query(
        'FIND TestRecordType WHICH HAS A TestTextProperty LIKE abc*efg',
        unique=True)
    assert rt2.id == rt.id

    rt2 = db.execute_query(
        'FIND TestRecordType WHICH HAS A TestTextProperty LIKE abc*g',
        unique=True)
    assert rt2.id == rt.id

    rt2 = db.execute_query(
        'FIND TestRecordType WHICH HAS A TestTextProperty LIKE *',
        unique=True)
    assert rt2.id == rt.id

    rt2 = db.execute_query(
        'FIND TestRecordType WHICH HAS A TestTextProperty LIKE a*',
        unique=True)
    assert rt2.id == rt.id

    rt2 = db.execute_query(
        'FIND TestRecordType WHICH HAS A TestTextProperty LIKE *abc*',
        unique=True)
    assert rt2.id == rt.id

    rt2 = db.execute_query(
        'FIND TestRecordType WHICH HAS A TestTextProperty LIKE *fg',
        unique=True)
    assert rt2.id == rt.id

    rt2 = db.execute_query(
        'FIND TestRecordType WHICH HAS A TestTextProperty LIKE a*f*g',
        unique=True)
    assert rt2.id == rt.id

    rt2 = db.execute_query(
        'FIND TestRecordType WHICH HAS A TestTextProperty LIKE a*d*g',
        unique=True)
    assert rt2.id == rt.id

    c = db.execute_query(
        'FIND TestRecordType WHICH HAS A TestTextProperty LIKE abc')
    assert 0 == len(c)

    c = db.execute_query(
        'FIND TestRecordType WHICH HAS A TestTextProperty = abc*')
    assert 0 == len(c)

    c = db.execute_query(
        'FIND TestRecordType WHICH HAS A TestTextProperty = *abc*')
    assert 0 == len(c)

    c = db.execute_query(
        'FIND TestRecordType WHICH HAS A TestTextProperty LIKE *f')
    assert 0 == len(c)


def store_file(path, name=None, f=None):
    """insert a file for testing purposes."""
    tmp_file = None
    if f is None:
        tmp_file = open("test.dat", "w")
        tmp_file.write("hello world\n")
        tmp_file.close()
        f = tmp_file
    if name is None:
        name = "TestFile%030x" % random.randrange(16**30)

    file_ = db.File(name=name,
                    description="Testfile Desc",
                    path=path,
                    file="test.dat")
    file_.insert()

    if tmp_file is not None:
        os.remove("test.dat")
    return file_


def test_stored_at_wildcards():

    with open("test.dat", "w") as upload_file:
        upload_file.write("hello world\n")

    file1 = store_file("test1.dat", f=upload_file)

    file2 = store_file("/rootdir/test2.dat", f=upload_file)

    file3 = store_file("/rootdir/subdir1/test3.dat", f=upload_file)

    file4 = store_file("/rootdir/subdir1/subdir2/test4.dat", f=upload_file)

    file5 = store_file("test5.dat", f=upload_file)

    file6 = store_file("rootdir/test*6.dat", f=upload_file)

    file7 = store_file("rootdir/subdir1/test%7.dat", f=upload_file)

    file8 = store_file("rootdir/subdir1/test%8.dat", f=upload_file)

    c = db.execute_query("FIND FILE WHICH IS STORED AT /*.dat")
    assert len(c) == 2
    assert c.get_entity_by_id(file1.id) is not None
    assert c.get_entity_by_id(file5.id) is not None

    c = db.execute_query("FIND FILE WHICH IS STORED AT /**/subdir*/*.dat")
    assert len(c) == 4
    assert c.get_entity_by_id(file3.id) is not None
    assert c.get_entity_by_id(file4.id) is not None
    assert c.get_entity_by_id(file7.id) is not None
    assert c.get_entity_by_id(file8.id) is not None

    c = db.execute_query("FIND FILE WHICH IS STORED AT *subdir**.dat")
    assert len(c) == 4
    assert c.get_entity_by_id(file3.id) is not None
    assert c.get_entity_by_id(file4.id) is not None
    assert c.get_entity_by_id(file7.id) is not None
    assert c.get_entity_by_id(file8.id) is not None

    c = db.execute_query("FIND FILE WHICH IS STORED AT *subdir1**.dat")
    assert len(c) == 4
    assert c.get_entity_by_id(file3.id) is not None
    assert c.get_entity_by_id(file4.id) is not None
    assert c.get_entity_by_id(file7.id) is not None
    assert c.get_entity_by_id(file8.id) is not None

    c = db.execute_query("FIND FILE WHICH IS STORED AT *subdir2**.dat")
    assert len(c) == 1
    assert c.get_entity_by_id(file4.id) is not None

    c = db.execute_query("FIND FILE WHICH IS STORED AT /rootdir/*.dat")
    assert len(c) == 2
    assert c.get_entity_by_id(file2.id) is not None
    assert c.get_entity_by_id(file6.id) is not None

    f = db.execute_query("FIND FILE WHICH IS STORED AT test1.dat", unique=True)
    assert f.id == file1.id

    f = db.execute_query(
        "FIND FILE WHICH IS STORED AT /test1.dat",
        unique=True)
    assert f.id == file1.id

    f = db.execute_query(
        "FIND FILE WHICH IS STORED AT *test1.dat",
        unique=True)
    assert f.id == file1.id

    f = db.execute_query(
        "FIND FILE WHICH IS STORED AT /*test1.dat",
        unique=True)
    assert f.id == file1.id

    f = db.execute_query("FIND FILE WHICH IS STORED AT *1.dat", unique=True)
    assert f.id == file1.id

    f = db.execute_query("FIND FILE WHICH IS STORED AT /*1.dat", unique=True)
    assert f.id == file1.id

    f = db.execute_query(
        "FIND FILE WHICH IS STORED AT **test1.dat",
        unique=True)
    assert f.id == file1.id

    f = db.execute_query(
        "FIND FILE WHICH IS STORED AT /**test1.dat",
        unique=True)
    assert f.id == file1.id

    f = db.execute_query(
        "FIND FILE WHICH IS STORED AT rootdir/test2.dat",
        unique=True)
    assert f.id == file2.id

    f = db.execute_query(
        "FIND FILE WHICH IS STORED AT /rootdir/test2.dat",
        unique=True)
    assert f.id == file2.id

    f = db.execute_query(
        "FIND FILE WHICH IS STORED AT *rootdir/test2.dat",
        unique=True)
    assert f.id == file2.id

    f = db.execute_query(
        "FIND FILE WHICH IS STORED AT /*rootdir/test2.dat",
        unique=True)
    assert f.id == file2.id

    f = db.execute_query(
        "FIND FILE WHICH IS STORED AT **rootdir/test2.dat",
        unique=True)
    assert f.id == file2.id

    f = db.execute_query(
        "FIND FILE WHICH IS STORED AT /**rootdir/test2.dat",
        unique=True)
    assert f.id == file2.id

    f = db.execute_query(
        "FIND FILE WHICH IS STORED AT *dir/test2.dat",
        unique=True)
    assert f.id == file2.id

    f = db.execute_query(
        "FIND FILE WHICH IS STORED AT /*dir/test2.dat",
        unique=True)
    assert f.id == file2.id

    f = db.execute_query(
        "FIND FILE WHICH IS STORED AT **dir/test2.dat",
        unique=True)
    assert f.id == file2.id

    f = db.execute_query(
        "FIND FILE WHICH IS STORED AT /**dir/test2.dat",
        unique=True)
    assert f.id == file2.id

    c = db.execute_query("FIND FILE WHICH IS STORED AT *.dat")
    assert len(c) == 8

    c = db.execute_query("FIND FILE WHICH IS STORED AT *test*.dat")
    assert len(c) == 8

    c = db.execute_query("FIND FILE WHICH IS STORED AT *test%*.dat")
    assert len(c) == 2
    assert c.get_entity_by_id(file7.id) is not None
    assert c.get_entity_by_id(file8.id) is not None

    f = db.execute_query(
        r"FIND FILE WHICH IS STORED AT *test\**.dat",
        unique=True)
    assert f.id == file6.id

    f = db.execute_query(
        r"FIND FILE WHICH IS STORED AT /rootdir/subdir1/subdir2/**",
        unique=True)
    assert f.id == file4.id

    f = db.execute_query(
        r"FIND FILE WHICH IS STORED AT '/rootdir/subdir1/subdir2/**'",
        unique=True)
    assert f.id == file4.id

    c = db.execute_query(
        r"FIND FILE WHICH IS STORED AT /rootdir/subdir1/**")
    assert len(c) == 4
    assert c.get_entity_by_id(file3.id) is not None
    assert c.get_entity_by_id(file4.id) is not None
    assert c.get_entity_by_id(file7.id) is not None
    assert c.get_entity_by_id(file8.id) is not None

    c = db.execute_query(
        r"FIND FILE WHICH IS STORED AT '/rootdir/subdir1/**'")
    assert len(c) == 4
    assert c.get_entity_by_id(file3.id) is not None
    assert c.get_entity_by_id(file4.id) is not None
    assert c.get_entity_by_id(file7.id) is not None
    assert c.get_entity_by_id(file8.id) is not None

    c = db.execute_query(
        r"FIND FILE WHICH IS STORED AT /rootdir/**")
    assert len(c) == 6
    assert c.get_entity_by_id(file2.id) is not None
    assert c.get_entity_by_id(file3.id) is not None
    assert c.get_entity_by_id(file4.id) is not None
    assert c.get_entity_by_id(file6.id) is not None
    assert c.get_entity_by_id(file7.id) is not None
    assert c.get_entity_by_id(file8.id) is not None

    c = db.execute_query(
        r"FIND FILE WHICH IS STORED AT '/rootdir/**'")
    assert len(c) == 6
    assert c.get_entity_by_id(file2.id) is not None
    assert c.get_entity_by_id(file3.id) is not None
    assert c.get_entity_by_id(file4.id) is not None
    assert c.get_entity_by_id(file6.id) is not None
    assert c.get_entity_by_id(file7.id) is not None
    assert c.get_entity_by_id(file8.id) is not None


@mark.slow
def test_int():
    pint = db.Property(
        name="TestIntegerProperty",
        datatype=db.INTEGER).insert()
    pdouble = db.Property(
        name="TestDoubleProperty",
        datatype=db.DOUBLE).insert()
    db.RecordType(
        name="TestRecordType").add_property(
        pint,
        value="100").add_property(
            pdouble,
        value="100.5").insert()

    db.execute_query(
        "FIND TestRecordType WITH TestIntegerProperty",
        unique=True)
    db.execute_query(
        "FIND TestRecordType WITH TestIntegerProperty>50",
        unique=True)
    db.execute_query(
        "FIND TestRecordType WITH TestIntegerProperty>=50",
        unique=True)
    db.execute_query(
        "FIND TestRecordType WITH TestIntegerProperty>=100",
        unique=True)
    db.execute_query(
        "FIND TestRecordType WITH TestIntegerProperty<=100",
        unique=True)
    db.execute_query(
        "FIND TestRecordType WITH TestIntegerProperty<=200",
        unique=True)
    db.execute_query(
        "FIND TestRecordType WITH TestIntegerProperty<200",
        unique=True)

    assert 0 == len(db.execute_query(
        "FIND TestRecordType WITH TestIntegerProperty>200"))
    assert 0 == len(db.execute_query(
        "FIND TestRecordType WITH TestIntegerProperty>=200"))
    assert 0 == len(db.execute_query(
        "FIND TestRecordType WITH TestIntegerProperty=200"))
    assert 0 == len(db.execute_query(
        "FIND TestRecordType WITH TestIntegerProperty<50"))
    assert 0 == len(db.execute_query(
        "FIND TestRecordType WITH TestIntegerProperty<=50"))

    db.execute_query(
        "FIND TestRecordType WITH TestDoubleProperty",
        unique=True)
    db.execute_query(
        "FIND TestRecordType WITH TestDoubleProperty>50",
        unique=True)
    db.execute_query(
        "FIND TestRecordType WITH TestDoubleProperty>=50",
        unique=True)
    db.execute_query(
        "FIND TestRecordType WITH TestDoubleProperty>=100.5",
        unique=True)
    db.execute_query(
        "FIND TestRecordType WITH TestDoubleProperty=100.5",
        unique=True)
    db.execute_query(
        "FIND TestRecordType WITH TestDoubleProperty<=100.5",
        unique=True)
    db.execute_query(
        "FIND TestRecordType WITH TestDoubleProperty<=200",
        unique=True)
    db.execute_query(
        "FIND TestRecordType WITH TestDoubleProperty<200",
        unique=True)

    assert 0 == len(db.execute_query(
        "FIND TestRecordType WITH TestDoubleProperty>200"))
    assert 0 == len(db.execute_query(
        "FIND TestRecordType WITH TestDoubleProperty>=200"))
    assert 0 == len(db.execute_query(
        "FIND TestRecordType WITH TestDoubleProperty=200"))
    assert 0 == len(db.execute_query(
        "FIND TestRecordType WITH TestDoubleProperty<50"))
    assert 0 == len(db.execute_query(
        "FIND TestRecordType WITH TestDoubleProperty<=50"))


def test_query_benchmark():
    db.Property("TestProperty", datatype=db.TEXT).insert()

    body = get_connection().retrieve(
        entity_uri_segments=["Entity"],
        query_dict={
            "query": "FIND TestProperty"},
        reconnect=True).read()
    print(body)
    xml = etree.fromstring(body)
    # see #4
    # assert 3 ==  len(xml)
    # assert "query" ==  xml[0].tag.lower()
    # assert "transactionbenchmark" ==  xml[0][3].tag.lower()
    # benchmark = xml[0][3]
    # assert len(benchmark) > 0


def test_like_query():
    """ See https://git.goltzsche.net/caosdb/customers/glaz_awi/ext-awi/issues/1
    """
    db.execute_query("FIND box with number like '**'")
    db.execute_query("FIND box with number like 'a'")
    db.execute_query("FIND box with number like '*7*'")
    db.execute_query("FIND box with number like '7*'")
    db.execute_query("FIND box with number like '*7'")
    db.execute_query("FIND box with number like '7'")
    db.execute_query("FIND box with number = '7'")


def test_backref_like():
    db.RecordType("TestData").insert()
    other = db.Record("other").add_parent("TestData").insert()
    sme = db.Record("s_m_e").add_property(
        "TestData", other.id).add_parent("TestData").insert()
    print(sme)
    db.execute_query("FIND ENTITY WHICH IS REFERENCED BY s_m_e", unique=True)
    db.execute_query("FIND ENTITY WHICH IS REFERENCED BY *m*", unique=True)
    db.execute_query("FIND ENTITY WHICH IS REFERENCED BY s*", unique=True)
    db.execute_query("FIND ENTITY WHICH IS REFERENCED BY *e", unique=True)


def test_query_by_name():
    rt = db.RecordType("TestRT").insert()
    rec1 = db.Record("TestRec1").add_parent("TestRT").insert()
    rec2 = db.Record("TestRec2").add_parent("TestRT").insert()

    assert len(db.execute_query("FIND TestRT")) == 3
    assert len(db.execute_query("FIND RECORD TestRT")) == 2
    assert len(db.execute_query("FIND RECORDTYPE TestRT")) == 1

    # find rt via name
    assert db.execute_query("FIND RECORDTYPE WITH name = 'TestRT'",
                            unique=True).id == rt.id
    assert db.execute_query("FIND RECORDTYPE WITH NAME = 'TestRT'",
                            unique=True).id == rt.id
    assert db.execute_query("FIND RECORDTYPE WITH NAme = 'TestRT'",
                            unique=True).id == rt.id
    assert db.execute_query("FIND TestRT WITH NAme = 'TestRT'",
                            unique=True).id == rt.id
    assert db.execute_query("FIND RECORDTYPE WITH name LIKE Test*",
                            unique=True).id == rt.id

    # find rec1 via name
    assert db.execute_query("FIND TestRT WITH name = 'TestRec1'",
                            unique=True).id == rec1.id
    assert db.execute_query("FIND RECORD TestRT WITH name = 'TestRec1'",
                            unique=True).id == rec1.id
    assert db.execute_query("FIND RECORD TestRT WITH name LIKE '*Rec1'",
                            unique=True).id == rec1.id

    # find both records via name
    assert len(db.execute_query("FIND TestRT WITH name LIKE 'TestRec*'")) == 2
    assert len(db.execute_query("FIND TestRT WITH name LIKE '*Rec*'")) == 2
    assert len(db.execute_query("FIND ENTITY WITH name LIKE 'TestRec*'")) == 2


@mark.xfail(reason="Issue: https://gitlab.com/caosdb/caosdb-server/-/issues/96")
def test_referenced_as():
    rt_person = db.RecordType("TestPerson").insert()
    db.RecordType("TestParty").insert()

    # three persons
    g1 = db.Record().add_parent("TestPerson").insert()
    g2 = db.Record().add_parent("TestPerson").insert()
    g3 = db.Record().add_parent("TestPerson").insert()
    guest_ids = set(g1.id, g2.id, g3.id)

    party = db.Record(
        "Diamond Jubilee of Elizabeth II").add_parent("TestParty")
    party.add_property(rt_person, datatype=db.LIST(rt_person), name="Guests",
                       value=[g1, g2, g3])
    party.insert()
    guests = db.execute_query("FIND RECORD TestPerson WHICH IS REFERENCED BY "
                              "TestParty AS Guests")
    assert len(guests) == 3
    assert set([e.id for e in guests]) == guest_ids


def test_query_cache():
    entity = db.RecordType("TestRT").insert()

    # fill cache
    query = db.Query("COUNT TestRT")
    assert query.cached is None
    response = query.execute()
    assert response == 1
    assert query.cached == False
    assert query.etag is not None
    etag = query.etag

    # cached == True
    query = db.Query("COUNT TestRT")
    assert query.cached is None
    response = query.execute()
    assert response == 1
    assert query.cached == True
    assert query.etag == etag

    # cached == True
    query = db.Query("FIND TestRT")
    assert query.cached is None
    response = query.execute(unique=True)
    assert response.id == entity.id
    assert query.cached == True
    assert query.etag == etag

    # cached == True
    query = db.Query("SELECT bla FROM TestRT")
    assert query.cached is None
    response = query.execute(unique=True)
    assert response.id == entity.id
    assert query.cached == True
    assert query.etag == etag

    # no cache flag
    query = db.Query("SELECT bla FROM TestRT")
    assert query.cached is None
    response = query.execute(unique=True, cache=False)
    assert response.id == entity.id
    assert query.cached == False
    assert query.etag == etag

    # cached == True
    query = db.Query("SELECT bla FROM TestRT")
    assert query.cached is None
    response = query.execute(unique=True)
    assert response.id == entity.id
    assert query.cached == True
    assert query.etag == etag

    # write resets cache
    another_entity = db.Record().add_parent("TestRT").insert()

    # cached == False
    query = db.Query("COUNT TestRT")
    assert query.cached is None
    response = query.execute()
    assert response == 2
    assert query.cached == False
    assert query.etag != etag
    new_etag = query.etag

    # cached == True
    query = db.Query("COUNT TestRT")
    assert query.cached is None
    response = query.execute()
    assert response == 2
    assert query.cached == True
    assert query.etag == new_etag


def test_query_cache_with_permissions():
    db.administration.set_server_property(
        "QUERY_FILTER_ENTITIES_WITHOUT_RETRIEVE_PERMISSIONS", "TRUE")
    db.RecordType("TestRT").insert()
    db.RecordType("TestRT2").insert()
    public_record = db.Record().add_parent("TestRT").insert()
    referenced = db.Record().add_parent("TestRT2").insert()
    private_record = db.Record().add_parent(
        "TestRT").add_property("TestRT2",
                               referenced).insert(flags={"ACL": None})

    # proof: query as anonymous works in principle
    db.configure_connection(password_method="unauthenticated")
    query = db.Query("COUNT Record TestRT")
    response = query.execute()
    assert response == 2
    assert query.cached == False

    query = db.Query("COUNT Record WHICH IS REFERENCED BY TestRT")
    response = query.execute()
    assert response == 1
    assert query.cached == False

    # remove permissions for anonymous
    db.configure_connection()
    private_record.deny(role="anonymous", permission="RETRIEVE:*")
    private_record.update_acl()  # note: this clears the cache

    # as authenticated user to fill the cache
    query = db.Query("COUNT Record TestRT")
    response = query.execute()
    assert response == 2
    assert query.cached == False

    query = db.Query("COUNT Record WHICH IS REFERENCED BY TestRT")
    response = query.execute()
    assert response == 1
    assert query.cached == False

    # as anonymous
    db.configure_connection(password_method="unauthenticated")
    query = db.Query("COUNT Record TestRT")
    response = query.execute()
    assert response == 1
    assert query.cached == True

    query = db.Query("COUNT Record WHICH IS REFERENCED BY TestRT")
    response = query.execute()
    assert response == 0
    # Caching was not possible due to complex permissions
    assert query.cached == False

    # try again as authenticated user
    db.configure_connection()
    query = db.Query("COUNT Record TestRT")
    response = query.execute()
    assert query.cached == True
    assert response == 2

    query = db.Query("COUNT Record WHICH IS REFERENCED BY TestRT")
    response = query.execute()
    assert response == 1
    # Caching was not possible due to complex permissions
    assert query.cached == False


def test_find_star():
    """related: caosdb-server#116"""
    rt = db.RecordType("TestRT").insert()
    response = db.execute_query("FIND ENTITY")[0]
    assert response.id == rt.id


def test_find_entity():
    rt = db.RecordType("TestRT").insert()
    response = db.execute_query("FIND ENTITY")[0]
    assert response.id == rt.id


def test_count_any_version_of_entity():
    vcount1 = db.execute_query("COUNT ANY VERSION OF ENTITY")
    ecount1 = db.execute_query("COUNT ENTITY")
    assert ecount1 == 0
    assert vcount1 == 0

    rt = db.RecordType("TestRT").insert()
    vcount2 = db.execute_query("COUNT ANY VERSION OF ENTITY")
    ecount2 = db.execute_query("COUNT ENTITY")
    assert vcount2 == vcount1 + 1
    assert ecount2 == ecount1 + 1

    rt.description = "update"
    rt.update()
    vcount3 = db.execute_query("COUNT ANY VERSION OF ENTITY")
    ecount3 = db.execute_query("COUNT ENTITY")
    assert vcount3 == vcount2 + 1
    assert ecount3 == ecount2


def test_find_nasty_path():
    """related: caosdb-server#31"""
    file_1 = store_file("0in.txt")
    assert db.execute_query(
        "FIND FILE WHICH IS STORED AT 0in.txt",
        unique=True).id == file_1.id

    file_2 = store_file(".asdf.txt")
    assert db.execute_query(
        "FIND FILE WHICH IS STORED AT .asdf.txt",
        unique=True).id == file_2.id

    file_3 = store_file(".WITH")
    assert db.execute_query(
        "FIND FILE WHICH IS STORED AT .WITH",
        unique=True).id == file_3.id

    file_4 = store_file("STORED")
    assert db.execute_query(
        "FIND FILE WHICH IS STORED AT STORED",
        unique=True).id == file_4.id

    file_5 = store_file("STORED AT")  # hehe
    assert db.execute_query(
        "FIND FILE WHICH IS STORED AT STORED AT",
        unique=True).id == file_5.id


def test_transaction_time():
    rt = db.RecordType("TestRT").insert(flags={"H": None})
    date = rt.version.date
    assert rt.id == db.execute_query(
        "FIND TestRT WHICH WAS INSERTED AT " + date,
        unique=True).id
    with raises(EmptyUniqueQueryError):
        db.execute_query("FIND TestRT WHICH WAS INSERTED AFTER " + date,
                         unique=True)
    with raises(EmptyUniqueQueryError):
        db.execute_query("FIND TestRT WHICH WAS INSERTED BEFORE " + date,
                         unique=True)
    assert rt.id == db.execute_query(
        "FIND TestRT WHICH WAS INSERTED SINCE " + date,
        unique=True).id
    assert rt.id == db.execute_query(
        "FIND TestRT WHICH WAS INSERTED UNTIL " + date,
        unique=True).id

    with raises(EmptyUniqueQueryError):
        db.execute_query("FIND TestRT WHICH WAS INSERTED AFTER TODAY",
                         unique=True)
    with raises(EmptyUniqueQueryError):
        db.execute_query("FIND TestRT WHICH WAS INSERTED BEFORE TODAY",
                         unique=True)
    assert rt.id == db.execute_query(
        "FIND TestRT WHICH WAS INSERTED SINCE TODAY",
        unique=True).id
    assert rt.id == db.execute_query(
        "FIND TestRT WHICH WAS INSERTED UNTIL TODAY",
        unique=True).id
    assert rt.id == db.execute_query(
        "FIND TestRT WHICH WAS INSERTED TODAY",
        unique=True).id

    year = date[:4]
    assert rt.id == db.execute_query(
        "FIND TestRT WHICH WAS INSERTED IN " + year,
        unique=True).id
    with raises(EmptyUniqueQueryError):
        db.execute_query("FIND TestRT WHICH WAS INSERTED AFTER " + year,
                         unique=True)
    with raises(EmptyUniqueQueryError):
        db.execute_query("FIND TestRT WHICH WAS INSERTED BEFORE " + year,
                         unique=True)
    assert rt.id == db.execute_query(
        "FIND TestRT WHICH WAS INSERTED SINCE " + year,
        unique=True).id
    assert rt.id == db.execute_query(
        "FIND TestRT WHICH WAS INSERTED UNTIL " + year,
        unique=True).id

    old_year = "2020"
    with raises(EmptyUniqueQueryError):
        db.execute_query(
            "FIND TestRT WHICH WAS INSERTED IN " + old_year,
            unique=True).id
    assert rt.id == db.execute_query(
        "FIND TestRT WHICH WAS INSERTED AFTER " + old_year,
        unique=True).id
    with raises(EmptyUniqueQueryError):
        db.execute_query("FIND TestRT WHICH WAS INSERTED BEFORE " + old_year,
                         unique=True)
    assert rt.id == db.execute_query(
        "FIND TestRT WHICH WAS INSERTED SINCE " + old_year,
        unique=True).id
    with raises(EmptyUniqueQueryError):
        db.execute_query("FIND TestRT WHICH WAS INSERTED UNTIL " + old_year,
                         unique=True)

    next_year = str(int(year) + 1)
    with raises(EmptyUniqueQueryError):
        db.execute_query(
            "FIND TestRT WHICH WAS INSERTED IN " + next_year,
            unique=True).id
    assert rt.id == db.execute_query(
        "FIND TestRT WHICH WAS INSERTED BEFORE " + next_year,
        unique=True).id
    with raises(EmptyUniqueQueryError):
        db.execute_query("FIND TestRT WHICH WAS INSERTED AFTER " + next_year,
                         unique=True)
    assert rt.id == db.execute_query(
        "FIND TestRT WHICH WAS INSERTED UNTIL " + next_year,
        unique=True).id
    with raises(EmptyUniqueQueryError):
        db.execute_query("FIND TestRT WHICH WAS INSERTED SINCE " + next_year,
                         unique=True)


def test_find_query_default_role():
    p = db.Property("TestProp", datatype=db.TEXT).insert()
    rt = db.RecordType("TestRT").add_property("TestProp").insert()
    rec = db.Record().add_parent("TestRT").add_property("TestProp", "val").insert()
    assert len(db.execute_query("FIND ENTITY Test*")) == 3

    db.administration.set_server_property("FIND_QUERY_DEFAULT_ROLE", "ENTITY")
    assert len(db.execute_query("FIND Test*")) == 3

    db.administration.set_server_property("FIND_QUERY_DEFAULT_ROLE", "PROPERTY")
    assert db.execute_query("FIND Test*", unique=True).id == p.id

    db.administration.set_server_property("FIND_QUERY_DEFAULT_ROLE",
                                          "RECORDTYPE")
    assert db.execute_query("FIND Test*", unique=True).id == rt.id

    db.administration.set_server_property("FIND_QUERY_DEFAULT_ROLE", "RECORD")
    assert db.execute_query("FIND Test*", unique=True).id == rec.id

    db.administration.set_server_property("FIND_QUERY_DEFAULT_ROLE", "asdf")
    with raises(TransactionError):
        assert db.execute_query("FIND Test*", unique=True).id == rec.id


def test_query_paging():
    test_query = "FIND RECORDTYPE TestRT*"

    # insert 20 entities
    for i in range(20):
        db.RecordType(f"TestRT{i}").insert()

    assert len(db.execute_query(test_query)) == 20

    # paging off
    assert isinstance(db.execute_query(test_query, page_length=0), db.Container)
    assert isinstance(db.execute_query(test_query, page_length=-1), db.Container)
    assert isinstance(db.execute_query(test_query, page_length=False), db.Container)
    assert isinstance(db.execute_query(test_query, page_length=None), db.Container)
    assert isinstance(
        db.execute_query(
            test_query +
            " WITH name=TestRT0",
            page_length=10,
            unique=True),
        db.RecordType)
    assert isinstance(db.execute_query("COUNT RECORDTYPE", page_length=10), int)

    # zero pages
    assert len([page for page in db.execute_query(
        test_query + " WITH unmet_condition", page_length=109)]) == 0
    # two pages
    assert len([page for page in db.execute_query(test_query, page_length=10)]) == 2
    assert len([page for page in db.execute_query(test_query, page_length=11)]) == 2
    assert len([page for page in db.execute_query(test_query, page_length=19)]) == 2
    # one page
    assert len([page for page in db.execute_query(test_query, page_length=20)]) == 1
    assert len([page for page in db.execute_query(test_query, page_length=100)]) == 1

    # concat
    assert len([entity for page in db.execute_query(test_query, page_length=10)
                for entity in page]) == 20

    # concat zero pages
    assert len([entity for page in db.execute_query(
        test_query + " WITH unmet_condition", page_length=10)
        for entity in page]) == 0

    # this kind of update is allowed
    for entity in [entity for page in db.execute_query(
            test_query, page_length=10) for entity in page]:
        entity.description = "updated"
        entity.update()

    # as well as this
    container = db.Container().extend(
        [entity for page in db.execute_query(test_query, page_length=10) for entity in page])
    for entity in container:
        entity.description = "updated 2"
    container.update()

    # but not this
    with raises(db.exceptions.PagingConsistencyError):
        for page in db.execute_query(test_query, page_length=10):
            for entity in page:
                entity.description = "update not allowed"
                entity.update()

    # not ideal: the first page has been written:
    pages = db.execute_query(test_query, page_length=10)
    for entity in next(pages):
        assert entity.description == "update not allowed"
    for entity in next(pages):
        assert entity.description == "updated 2"

    # the above pattern is ok if you don't write, tho
    for page in db.execute_query(test_query, page_length=10):
        for entity in page:
            assert entity.description is not None
            # don't: entity.update()


def test_greatest_smallest_id():
    rt1 = db.RecordType("TestRT1").insert()
    rt2 = db.RecordType("TestRT2").insert()

    # assumptions
    assert rt1.id < rt2.id

    # actual testing
    assert db.execute_query(
        "FIND RECORDTYPE Test* WITH ID>99 AND THE SMALLEST ID",
        unique=True).id == rt1.id
    assert db.execute_query("FIND RECORDTYPE Test* WITH THE GREATEST ID", unique=True).id == rt2.id

    assert db.execute_query("FIND ENTITY Test* WITH THE SMALLEST ID", unique=True).id == rt1.id
    assert db.execute_query("FIND ENTITY Test* WITH THE GREATEST ID", unique=True).id == rt2.id


@mark.xfail(reason="Issue: https://gitlab.com/linkahead/linkahead-server/-/issues/237")
def test_greatest_smallest_id_over_roles():
    rt1 = db.RecordType("TestRT1").insert()
    rt2 = db.RecordType("TestRT2").insert()
    p1 = db.Property("TestProp1", datatype=db.TEXT).insert()
    p2 = db.Property("TestProp2", datatype=db.TEXT).insert()

    # assumptions
    assert rt1.id < rt2.id
    assert rt2.id < p1.id
    assert p1.id < p2.id

    # actual testing
    assert db.execute_query("FIND RECORDTYPE WITH THE SMALLEST ID", unique=True).id == rt1.id
    assert db.execute_query("FIND RECORDTYPE WITH THE GREATEST ID", unique=True).id == rt2.id

    assert db.execute_query("FIND PROPERTY WITH THE SMALLEST ID", unique=True).id == p1.id
    assert db.execute_query("FIND PROPERTY WITH THE GREATEST ID", unique=True).id == p2.id

    assert db.execute_query("FIND ENTITY WITH THE SMALLEST ID", unique=True).id == rt1.id
    assert db.execute_query("FIND ENTITY WITH THE GREATEST ID", unique=True).id == p2.id


def test_find_id():
    """See also: https://gitlab.indiscale.com/caosdb/src/caosdb-server/-/issues/323"""
    rt1 = db.RecordType("TestRT1").insert()
    rt2 = db.RecordType("TestRT2").insert()
    rt3 = db.RecordType("TestRT3").add_parent("TestRT2").add_property("TestRT1").insert()

    assert db.execute_query("FIND TestRT1", unique=True).id == rt1.id
    assert db.execute_query(f"FIND {rt1.id}", unique=True).id == rt1.id
    assert set([e.id for e in db.execute_query("FIND TestRT2")]) == set([rt2.id, rt3.id])
    assert db.execute_query("FIND TestRT2 WITH TestRT1", unique=True).id == rt3.id
    assert db.execute_query("FIND TestRT3 WITH TestRT1", unique=True).id == rt3.id
    assert db.execute_query(f"FIND {rt2.id} WITH TestRT1", unique=True).id == rt3.id
    assert db.execute_query(f"FIND {rt3.id} WITH TestRT1", unique=True).id == rt3.id


def test_get_entity():
    person = db.RecordType("TestPerson").insert()
    db.Property("TestFirstName", datatype=db.TEXT).insert()
    con = db.Property("TestConductor", datatype=db.REFERENCE).insert()
    db.RecordType("TestConductor").insert(unique=False)

    # create testfile
    f = open("test.dat", "w")
    f.write("hello world\n")
    f.close()

    ''' prepare file record '''
    f = db.File(name="TestFile", path='testfiles/testfile', file="test.dat")
    f.insert()

    assert get_entity_by_id(person.id).id == person.id
    assert get_entity_by_name(person.name).id == person.id
    assert get_entity_by_id(person.id, role="RecordType").id == person.id
    assert get_entity_by_name("TestConductor", role="Property").id == con.id
    assert get_entity_by_path(f.path).id == f.id
