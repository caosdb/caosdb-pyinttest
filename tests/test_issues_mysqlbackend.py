# -*- coding: utf-8 -*-
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (c) 2020 IndiScale GmbH (www.indiscale.com)
# Copyright (c) 2020 Daniel Hornung (d.hornung@indiscale.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header

"""Tests for issues on gitlab.com, project caosdb-mysqlbackend."""

import caosdb as db


def setup_module():
    try:
        db.execute_query("FIND ENTITY WITH ID > 100").delete()
    except Exception as delete_exc:
        print(delete_exc)


def setup_function(function):
    """No setup required."""
    setup_module()


def teardown_function(function):
    """Deleting entities again."""
    setup_module()


# ########################### Issue tests start here #####################

def test_issue_18():
    """Duplicate parents were returned in some cases of self-hineritance.

    Tests for https://gitlab.com/caosdb/caosdb-mysqlbackend/-/issues/18
    """
    A = db.RecordType(name="A")
    B = db.RecordType(name="B")
    C = db.RecordType(name="C")

    B.add_parent(A)
    C.add_parent(B)
    C.add_parent(C)  # The self-parenting is here on purpose
    C.add_parent(A)

    cont = db.Container()
    cont.extend([B, C, A])
    cont.insert()

    C1 = db.Entity(name="C").retrieve()
    pids = [p.id for p in C1.parents]
    assert len(set(pids)) == len(pids), "Duplicate parents."


def test_issue_21():
    """Removing the last child in an inheritance chain of 4 failed with versioning.

    Tests for https://gitlab.com/caosdb/caosdb-mysqlbackend/-/issues/21
    """
    A = db.RecordType(name="A")
    B = db.RecordType(name="B")
    C = db.RecordType(name="C")
    rec = db.RecordType(name="rec")

    A.add_parent(B)
    B.add_parent(C)
    rec.add_parent(A)

    cont = db.Container()
    cont.extend([A, B, C, rec])
    cont.insert()

    rec.delete()
