# encoding: utf-8
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2021 IndiScale GmbH <info@indiscale.com>
# Copyright (C) 2021 Alexander Kreft
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#

import unittest
import caosdb as db
from caosdb.utils.plantuml import retrieve_substructure


class TestRetrieveSubstructure(unittest.TestCase):
    test_prefix = "TST_substructure:"

    def setUp(self):
        prop1 = db.Property(
            name=f'{self.test_prefix}P1-1', datatype=db.INTEGER)
        prop2 = db.Property(
            name=f'{self.test_prefix}P1-2', datatype=db.INTEGER)
        prop3 = db.Property(
            name=f'{self.test_prefix}P1-3', datatype=f'{self.test_prefix}RT2')
        prop4 = db.Property(
            name=f'{self.test_prefix}P2-1', datatype=db.INTEGER)
        prop5 = db.Property(
            name=f'{self.test_prefix}P2-2', datatype=db.INTEGER)
        prop6 = db.Property(
            name=f'{self.test_prefix}P3-1', datatype=db.INTEGER)

        rt1 = db.RecordType(f'{self.test_prefix}RT1')
        rt2 = db.RecordType(f'{self.test_prefix}RT2')
        rt3 = db.RecordType(f'{self.test_prefix}RT3')
        rt1.add_property(prop1)
        rt1.add_property(prop2)
        rt1.add_property(prop3)
        rt2.add_property(prop4)
        rt2.add_property(prop5)
        rt2.add_property(rt3)
        rt3.add_property(prop6)

        rt4 = db.RecordType(f'{self.test_prefix}RT4')
        rt5 = db.RecordType(f'{self.test_prefix}RT5')
        rt6 = db.RecordType(f'{self.test_prefix}RT6')
        rt7 = db.RecordType(f'{self.test_prefix}RT7')
        rt5.add_parent(rt4)
        rt6.add_parent(rt5)
        rt7.add_parent(rt4)
        rt7.add_parent(rt5)

        rt8 = db.RecordType(f'{self.test_prefix}RT8')
        rt8.add_parent(rt6)
        rt8.add_property(rt1)

        ins = db.Container()
        for i in [rt1, rt2, rt3, rt4, rt5, rt6, rt7, rt8,
                  prop1, prop2, prop3, prop4, prop5, prop6]:
            ins.append(i)
        ins.insert()

    def test_substructure_childs(self):
        rt = db.RecordType(f'{self.test_prefix}RT1').retrieve()

        tst0 = retrieve_substructure([rt], -1)  # same as 0
        tst1 = retrieve_substructure([rt], 0)
        tst2 = retrieve_substructure([rt], 1)
        tst3 = retrieve_substructure([rt], 2)
        tst4 = retrieve_substructure([rt], 3)  # same as 2

        self.assertEqual(len(tst0), len(tst1))
        self.assertEqual(len(tst1), 4)
        self.assertEqual(len(tst2), 8)
        self.assertEqual(len(tst3), 9)
        self.assertEqual(len(tst3), len(tst4))

        rt = db.RecordType(f'{self.test_prefix}RT2').retrieve()
        tst1 = retrieve_substructure([rt], 0)
        tst2 = retrieve_substructure([rt], 1)

        self.assertEqual(len(tst1), 4)
        self.assertEqual(len(tst2), 5)

        rt = db.RecordType(f'{self.test_prefix}RT3').retrieve()
        tst1 = retrieve_substructure([rt], 0)
        tst2 = retrieve_substructure([rt], 1)

        self.assertEqual(len(tst1), 2)
        self.assertEqual(len(tst2), 2)

        # test lists
        rt1 = db.RecordType(f'{self.test_prefix}RT1').retrieve()
        rt2 = db.RecordType(f'{self.test_prefix}RT2').retrieve()
        rt3 = db.RecordType(f'{self.test_prefix}RT3').retrieve()
        rt_list1 = [rt1, rt2]
        rt_list2 = [rt1, rt3]
        rt_list3 = [rt2, rt3]
        rt_list4 = [rt1, rt2, rt3]

        tst_list10 = retrieve_substructure(rt_list1, 0)
        tst_list11 = retrieve_substructure(rt_list1, 1)
        tst_list12 = retrieve_substructure(rt_list1, 2)
        self.assertEqual(len(tst_list10), 8)
        self.assertEqual(len(tst_list11), 9)
        self.assertEqual(len(tst_list12), 9)

        tst_list20 = retrieve_substructure(rt_list2, 0)
        tst_list21 = retrieve_substructure(rt_list2, 1)
        tst_list22 = retrieve_substructure(rt_list2, 2)
        self.assertEqual(len(tst_list20), 6)
        self.assertEqual(len(tst_list21), 9)
        self.assertEqual(len(tst_list22), 9)

        tst_list30 = retrieve_substructure(rt_list3, 0)
        tst_list31 = retrieve_substructure(rt_list3, 1)
        tst_list32 = retrieve_substructure(rt_list3, 2)
        self.assertEqual(len(tst_list30), 5)
        self.assertEqual(len(tst_list31), 5)
        self.assertEqual(len(tst_list32), 5)

        tst_list40 = retrieve_substructure(rt_list4, 0)
        tst_list41 = retrieve_substructure(rt_list4, 1)
        tst_list42 = retrieve_substructure(rt_list4, 2)
        self.assertEqual(len(tst_list40), 9)
        self.assertEqual(len(tst_list41), 9)
        self.assertEqual(len(tst_list42), 9)

    def test_substructure_parents(self):
        rt1 = db.RecordType(f'{self.test_prefix}RT4').retrieve()
        rt2 = db.RecordType(f'{self.test_prefix}RT5').retrieve()
        rt3 = db.RecordType(f'{self.test_prefix}RT6').retrieve()
        rt4 = db.RecordType(f'{self.test_prefix}RT7').retrieve()

        tst1 = retrieve_substructure([rt1], 0)
        tst2 = retrieve_substructure([rt1], -1)
        tst3 = retrieve_substructure([rt1], 2)
        tst4 = retrieve_substructure([rt1], 1024)
        self.assertEqual(len(tst1), 1)
        self.assertEqual(len(tst2), 1)
        self.assertEqual(len(tst3), 1)
        self.assertEqual(len(tst4), 1)

        tst1 = retrieve_substructure([rt2], 0)
        tst2 = retrieve_substructure([rt2], -1)
        tst3 = retrieve_substructure([rt2], 2)
        tst4 = retrieve_substructure([rt2], 1024)
        self.assertEqual(len(tst1), 2)
        self.assertEqual(len(tst2), 2)
        self.assertEqual(len(tst3), 2)
        self.assertEqual(len(tst4), 2)

        tst1 = retrieve_substructure([rt3], 0)
        tst2 = retrieve_substructure([rt3], 1)
        tst3 = retrieve_substructure([rt3], 2)
        tst4 = retrieve_substructure([rt3], 1024)
        self.assertEqual(len(tst1), 2)
        self.assertEqual(len(tst2), 3)
        self.assertEqual(len(tst3), 3)
        self.assertEqual(len(tst4), 3)

        """
        Parent tree:
        7
        ├── 4
        └── 5
            └── 4
        """
        tst1 = retrieve_substructure([rt4], 0)
        tst2 = retrieve_substructure([rt4], 1)
        tst3 = retrieve_substructure([rt4], 2)
        tst4 = retrieve_substructure([rt4], 1024)
        self.assertEqual(len(tst1), 3)
        self.assertEqual(len(tst2), 3)
        self.assertEqual(len(tst3), 3)
        self.assertEqual(len(tst4), 3)

    def test_substructure_mixed(self):
        rt = db.RecordType(f'{self.test_prefix}RT8').retrieve()
        tst1 = retrieve_substructure([rt], 0)
        tst2 = retrieve_substructure([rt], 1)
        tst3 = retrieve_substructure([rt], 2)
        tst4 = retrieve_substructure([rt], 3)

        """
        Parent tree:
        8
        └── 6
            └── 5
                └── 4
        Properties Tree:
        8
        1
        ├── 1
        ├── int
        ├── int
        └── 2 (Prop)
            ├── 2
            ├── int
            ├── int
            └── 3
                ├── 3
                └── int
        """
        self.assertEqual(len(tst1), 3)  # 8, 6, 1
        self.assertEqual(len(tst2), 7)  # 8, 6, 5, 1, int, int, 2 (prop)
        # 8, 6, 5, 4, 1, int, int, 2 (prop), 2, int, int, 3
        self.assertEqual(len(tst3), 12)
        # 8, 6, 5, 4, 1, int, int, 2 (prop), 2, int, int, 3, int
        self.assertEqual(len(tst4), 13)

    def tearDown(self) -> None:
        db.execute_query(f'FIND ENTITY {self.test_prefix}*').delete()


if __name__ == '__main__':
    unittest.main()
