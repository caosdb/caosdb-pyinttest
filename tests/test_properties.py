# -*- coding: utf-8 -*-
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (c) 2020 IndiScale GmbH <www.indiscale.com>
# Copyright (c) 2020 Timm Fitschen <t.fitschen@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
import pytest

import caosdb as db
from caosdb.common.utils import xml2str


def setup_module():
    d = db.execute_query("FIND Test*")
    if len(d) > 0:
        d.delete()


def setup_function(function):
    setup_module()


def teardown_function(function):
    setup_module()


def test_add_properties_with_wrong_role():
    p = db.Property(name="TestProperty1", datatype=db.TEXT).insert()
    rt = db.RecordType(name="TestRT1").add_property("TestProperty1").insert()

    no_role = db.Entity(name="TestRT1").retrieve()
    no_role.role = None
    wrong_role = db.Entity(name="TestRT1").retrieve()
    wrong_role.role = "Record"
    right_role = db.RecordType(name="TestRT1").retrieve()

    with pytest.raises(ValueError):
        rec = db.Record().add_parent("TestRT1").add_property(wrong_role)
    rec2 = db.Record().add_parent("TestRT1").add_property(right_role)
    rec3 = db.Record().add_parent("TestRT1").add_property(no_role)

    assert wrong_role.get_property("TestProperty1") is not None
    assert str(wrong_role.get_property("TestProperty1")) == str(
        right_role.get_property("TestProperty1"))

    xml = rec2.to_xml()
    assert not xml.xpath("/Record/Property/Property")

    xml = rec3.to_xml()
    assert not xml.xpath("/Record/Property/Property")

    rec2.insert()
    rec3.insert()

    for e in [rec2, rec3]:
        assert e.id is not None
        assert e.get_property("TestRT1") is not None
        assert e.get_property("TestRT1").value is None
