#!/usr/bin/python2
# encoding: utf-8
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#

# Testcase fuer insertion
# A. Schlemmer, 08/2014

from caosdb import Container, RecordType, Property, execute_query


def setup_function(function):
    try:
        execute_query("FIND PA_*").delete()
    except BaseException:
        pass


def test_PA_Datamodel():
    pr10 = Property(name="PA_DataFile", id=-10, datatype="file",
                    description="The property specifying the "
                    "file inside the workspace.")
    pr11 = Property(name="PA_WorkspaceFile", id=-11, datatype="file",
                    description="The property specifying the "
                    "workspace file containing meta-information about "
                    "each PythonAnalyser workspace.")
    pr12 = Property(name="PA_LogEntryReference", id=-12, datatype=-1,
                    description="The log entries of a PythonAnalyser "
                    "workspace are connected (e.g. via plugin "
                    "function operations). "
                    "These connections are specified using this property.")
    pr13 = Property(name="PA_WorkspaceReference", id=-13, datatype=-9,
                    description="Specifies the relation of log entries to "
                    "PythonAnalyser workspaces.")

    rt1 = RecordType(name="PA_LogEntry", id=-1,
                     description="The base RecordType for "
                     "all \"loggable\" things in PythonAnalyser.")
    rt2 = RecordType(name="PA_Dataset", id=-2,
                     description="Base RecordType for"
                     "PythonAnalyser datasets.")
    rt3 = RecordType(name="PA_Visualizer", id=-3,
                     description="PythonAnalyser visualization "
                     "plugins.")
    rt4 = RecordType(name="PA_Function", id=-4,
                     description="PythonAnalyser function plugins")
    rt5 = RecordType(name="PA_Table", id=-5,
                     description="PythonAnalyser dataset type for "
                     "tabular data using pandas as backend.")
    rt6 = RecordType(name="PA_Numeric", id=-6,
                     description="PythonAnalyser dataset type for "
                     "different types of numeric data using numpy "
                     "arrays as backend.")
    rt7 = RecordType(name="PA_Importer", id=-7,
                     description="Base RecordType for plugins "
                     "which import data into PythonAnalyser.")
    rt8 = RecordType(name="PA_Exporter", id=-8,
                     description="Base RecordType for plugins "
                     "which export data from PythonAnalyser.")
    rt9 = RecordType(name="PA_Workspace", id=-9,
                     description="RecordType for PythonAnalyser "
                     "workspaces.")
    rts = [rt1, rt2, rt3, rt4, rt5, rt6, rt7, rt8, rt9]

    # Adding the properties:
    rt5.add_property(pr10, importance="OBLIGATORY")
    rt6.add_property(pr10, importance="OBLIGATORY")
    rt9.add_property(pr11, importance="OBLIGATORY")
    for i in rts:
        if i != rt9:
            i.add_property(pr13, importance="OBLIGATORY")

    # Defining the structure:
    rt2.add_parent(rt1)
    rt3.add_parent(rt1)
    rt4.add_parent(rt1)
    rt5.add_parent(rt2)
    rt6.add_parent(rt2)
    rt7.add_parent(rt4)
    rt8.add_parent(rt4)

    # Inserting it all:
    c = Container()
    c.extend(rts)
    c.extend([pr10, pr11, pr12, pr13])
    # c.appen(pr14)
    c.insert()

    c.delete()
