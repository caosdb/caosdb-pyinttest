# encoding: utf-8
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
"""Created on 16.07.2015.

@author: tf
"""
from pytest import mark
from lxml import etree

import caosdb as db
from caosdb.connection.connection import get_connection
from caosdb.common.utils import xml2str
from caosdb.common.models import raise_errors


def setup_function(function):
    d = db.execute_query("FIND Entity WITH ID > 99")
    if len(d) > 0:
        d.delete()


def teardown_function(function):
    setup_function(function)


def test_property_no_id():
    p1 = db.Property(name="P1", datatype=db.TEXT).insert()
    rt1 = db.RecordType(name="RT1").insert()

    rt1.add_property(name="P1").update(raise_exception_on_error=False)
    assert rt1.get_property("P1").get_errors()[0].description \
        == "Entity has no ID."


def test_parent_no_id():
    child = db.RecordType(name="RTC").insert()
    parent = db.RecordType(name="RTP").insert()

    child.add_parent(name="RTP").update(raise_exception_on_error=False)
    assert child.get_parent("RTP").get_errors()[0].description \
        == "Entity has no ID."


def test_update_1():
    p1 = db.Property(name="FirstName", datatype='TEXT').insert()
    assert p1.is_valid()
    p2 = db.Property(name="LastName", datatype='TEXT').insert()
    assert p2.is_valid()
    p3 = db.Property(name="StartDate", datatype='DATETIME').insert()
    assert p3.is_valid()

    rt1 = db.RecordType(
        name="Person",
        description="A natural person.").add_property(
        p1,
        importance='OBLIGATORY').add_property(
        p2,
        importance='OBLIGATORY').insert()
    assert rt1.is_valid()
    assert 2 == len(rt1.get_properties())

    rt1_xml = rt1.to_xml()
    p3_xml = etree.Element("Property")
    p3_xml.set("id", str(p3.id))
    p3_xml.set("importance", "OBLIGATORY")
    rt1_xml.append(p3_xml)
    xml_str = '<Update>' + xml2str(rt1_xml) + '</Update>'

    con = get_connection()

    response = con.update(entity_uri_segment=["Entity"], body=xml_str)

    c = db.Container._response_to_entities(response)
    raise_errors(c)

    rt1.retrieve()
    assert rt1.is_valid()
    assert 3 == len(rt1.get_properties())


@mark.xfail(reason="TODO: investigate whats the problem here")
def test_server_error_during_update():
    rt1 = db.RecordType(name="TestRT1").insert()
    rt2 = db.RecordType(name="TestRT2").insert()
    p = db.Property(name="TestProperty", datatype="TestRT1").insert()
    p2 = db.Property(name="TestBogusProperty", datatype=db.TEXT).insert()
    rec1 = db.Record(name="TestRT1Rec1").add_parent("TestRT1").insert()
    rec2 = db.Record(name="TestRT2Rec2").add_parent("TestRT2").add_property(
        "TestProperty", "TestRT1Rec1").insert()

    # do stupid things
    rec2.get_property("TestProperty").add_property("TestBogusProperty")
    rec2.update()
