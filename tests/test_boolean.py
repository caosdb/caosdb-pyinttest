# encoding: utf-8
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
"""Created on 13.01.2016.

@author: tf
"""

import caosdb as h
from caosdb.exceptions import TransactionError
from pytest import raises


def teardown_module():
    try:
        h.execute_query("FIND ENTITY").delete()
    except BaseException:
        pass


def setup_function(function):
    teardown_module()


def test_property():
    p = h.Property(
        name="SimpleBooleanProperty",
        datatype=h.BOOLEAN).insert()
    assert p.is_valid()
    assert p.datatype == h.BOOLEAN

    p2 = h.Property(id=p.id).retrieve()
    assert p2.is_valid
    assert p2.datatype == h.BOOLEAN


def test_recordType():
    p = h.Property(
        name="SimpleBooleanProperty",
        datatype=h.BOOLEAN).insert()
    assert p.is_valid()

    rt = h.RecordType(name="SimpleRecordType").add_property(p).insert()
    assert rt.is_valid()
    assert rt.get_property("SimpleBooleanProperty").datatype == h.BOOLEAN

    rt2 = h.RecordType(id=rt.id).retrieve()
    assert rt2.is_valid()
    assert rt2.get_property("SimpleBooleanProperty").datatype == h.BOOLEAN


def test_record():
    p = h.Property(
        name="SimpleBooleanProperty",
        datatype=h.BOOLEAN).insert()
    assert p.is_valid()

    rt = h.RecordType(name="SimpleRecordType").add_property(p).insert()
    assert rt.is_valid()

    rec1 = h.Record(name="SimpleRecord1").add_parent(
        rt).add_property(p, value="TRUE").insert()
    assert rec1.is_valid()
    assert rec1.get_property("SimpleBooleanProperty").datatype == h.BOOLEAN
    assert rec1.get_property("SimpleBooleanProperty").value is True

    rec1c = h.Record(id=rec1.id).retrieve()
    assert rec1c.is_valid()
    assert rec1c.get_property("SimpleBooleanProperty").datatype == h.BOOLEAN
    assert rec1c.get_property("SimpleBooleanProperty").value is True

    rec2 = h.Record(name="SimpleRecord2").add_parent(
        rt).add_property(p, value=True).insert()
    assert rec2.is_valid()
    assert rec2.get_property("SimpleBooleanProperty").datatype == h.BOOLEAN
    assert rec2.get_property("SimpleBooleanProperty").value is True

    rec2c = h.Record(id=rec2.id).retrieve()
    assert rec2c.is_valid()
    assert rec2c.get_property("SimpleBooleanProperty").datatype == h.BOOLEAN
    assert rec2c.get_property("SimpleBooleanProperty").value is True

    rec3 = h.Record(
        name="SimpleRecord3").add_parent(rt).add_property(
        p.name, value="BLABLA")
    with raises(TransactionError):
        rec3.insert()

    assert not rec3.is_valid()
    assert rec3.get_property("SimpleBooleanProperty").get_errors()[
        0].description, ("Cannot parse value to boolean "
                         "(either 'true' or 'false' ==  " "case insensitive).")
