# -*- coding: utf-8 -*-
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
"""Created on 21.01.2015.

@author: tf
"""


import caosdb as db


def setup_function(function):
    teardown_function(function)


def teardown_function(function):
    d = db.execute_query("FIND ENTITY WITH ID > 99")
    if len(d) > 0:
        d.delete()


def test_inheritance_fix_properties():
    try:
        """FIX PROPERTIES."""
        '''insert simple property with unit'''
        p1 = db.Property(name='UnitTestProperty', datatype='double', unit='m')
        p1.insert()

        p1c = db.Property(id=p1.id).retrieve()
        assert p1c.is_valid()
        assert 'm' == p1c.unit

        '''subtyping with unit inheritance'''
        p2 = db.Property(
            name='SubTypeOfUnitTestProperty').add_parent(
            id=p1.id, inheritance="FIX")
        print(p2)
        p2.insert()
        print(p2)
        assert p2.is_valid()
        assert 'm' == p2.unit
    finally:
        try:
            p2.delete()
        except BaseException:
            pass
        try:
            p1.delete()
        except BaseException:
            pass


def test_inheritance_obl_properties():
    try:
        """OBLIGATORY PROPERTIES."""
        c = db.Container()
        c.append(
            db.Property(
                name="SimpleTextProperty",
                description="simple text property (from test_inheritance.py)",
                datatype='text'))
        c.append(
            db.Property(
                name="SimpleDoubleProperty",
                description="simple double property (from test_inheritance.py)",
                datatype='double'))
        c.append(
            db.Property(
                name="SimpleIntegerProperty",
                description="simple integer property (from test_inheritance.py)",
                datatype='integer'))
        c.append(
            db.Property(
                name="SimpleDatetimeProperty",
                description="simple datetime property (from test_inheritance.py)",
                datatype='datetime'))
        c.append(
            db.Property(
                name="SimpleFileProperty",
                description="simple file property (from test_inheritance.py)",
                datatype='file'))

        rt = db.RecordType(
            name="SimpleRecordType",
            description="simple recordType (from test_inheritance.py)")
        rt.add_property(name='SimpleTextProperty', importance="obligatory")
        rt.add_property(name='SimpleDoubleProperty', importance="obligatory")
        rt.add_property(name='SimpleIntegerProperty', importance="obligatory")
        rt.add_property(name='SimpleDatetimeProperty')
        rt .add_property(name='SimpleFileProperty')
        c.append(rt)
        c.insert()

        rt = db.RecordType(
            name="SubTypeOfSimpleRecordType",
            description="recordtype with inheritance (from test_inheritance.py)").add_parent(
            name="SimpleRecordType",
            inheritance="obligatory")
        rt.insert()
        # only three properties are obligatory
        assert 3 == len(rt.get_properties())
    finally:
        try:
            rt.delete()
        except BaseException:
            pass
        try:
            c.delete()
        except BaseException:
            pass


def test_inheritance_all_properties():
    """ALL PROPERTIES."""
    c = db.Container()
    c.append(
        db.Property(
            name="SimpleTextProperty",
            description="simple text property (from test_inheritance.py)",
            datatype='text'))
    c.append(
        db.Property(
            name="SimpleDoubleProperty",
            description="simple double property (from test_inheritance.py)",
            datatype='double'))
    c.append(
        db.Property(
            name="SimpleIntegerProperty",
            description="simple integer property (from test_inheritance.py)",
            datatype='integer'))
    c.append(
        db.Property(
            name="SimpleDatetimeProperty",
            description="simple datetime property (from test_inheritance.py)",
            datatype='datetime'))
    c.append(
        db.Property(
            name="SimpleFileProperty",
            description="simple file property (from test_inheritance.py)",
            datatype='file'))

    c.append(
        db.RecordType(
            name="SimpleRecordType",
            description="simple recordType (from test_inheritance.py)") .add_property(
            name='SimpleTextProperty',
            importance="obligatory") .add_property(
            name='SimpleDoubleProperty',
            importance="obligatory") .add_property(
                name='SimpleIntegerProperty',
                importance="obligatory") .add_property(
                    name='SimpleDatetimeProperty') .add_property(
                        name='SimpleFileProperty'))
    c.insert()

    rt = db.RecordType(
        name="SubTypeOfSimpleRecordType",
        description="recordtype with inheritance (from test_inheritance.py)").add_parent(
        name="SimpleRecordType",
        inheritance="obligatory")
    rt.insert()
    assert 3 == len(rt.get_properties())


def test_inheritance_unit():
    p = db.Property(
        name="SimpleIntProperty",
        datatype="INTEGER",
        unit="m")
    p.insert()
    assert p.is_valid()
    assert p.unit == "m"

    rt = db.RecordType(
        name="SimpleRecordType")
    rt.add_property(p, unit="km")
    rt.insert()

    assert rt.is_valid()
    assert rt.get_property("SimpleIntProperty").unit == "km"

    rt2 = db.execute_query("FIND ENTITY SimpleRecordType", True)
    assert rt2.id == rt.id
    assert rt2.get_property("SimpleIntProperty").unit == "km"

    rt3 = db.RecordType(
        name="SimpleRecordType2")
    rt3.add_parent(rt, inheritance="ALL")
    rt3.insert()
    assert rt3.is_valid()
    assert rt3.get_property("SimpleIntProperty") is not None
    assert rt3.get_property("SimpleIntProperty").unit == "km"

    rt4 = db.execute_query("FIND ENTITY SimpleRecordType2", True)
    assert rt4.is_valid()
    assert rt4.id == rt3.id
    assert rt4.get_property("SimpleIntProperty").unit == "km"

    rec = db.Record(
        name="SimpleRecord")
    rec.add_parent(rt3)
    rec.add_property(name="SimpleIntProperty", value=1)
    rec.insert()
    assert rec.is_valid()
    assert rec.get_property("SimpleIntProperty") is not None
    assert rec.get_property("SimpleIntProperty").unit == "km"


_ENTITIES = [
    db.RecordType(name="Simulation").add_property(name="SimulationModel"),

    db.RecordType(name="PublicationReference").add_property(name="date"),
    db.RecordType(
        name="SimulationModel").add_property(
        name="PublicationReference"),

    db.Property(name="date", datatype=db.TEXT),
    db.Property(name="blub", datatype=db.TEXT),
]


def test_inherit_subproperties():
    con = db.Container().extend(_ENTITIES)
    con.insert()
    valid = db.Container().extend(_ENTITIES).retrieve()
    container = db.Container().extend(_ENTITIES)
    container.get_entity_by_name("Simulation").add_property(name="blub")
    for valid_e in valid:
        for entity in container:
            for prop in entity.get_properties():
                if valid_e.name == prop.name:
                    prop.id = valid_e.id
                    entity.get_properties()._inheritance[prop] = db.ALL
    container.get_entity_by_name("SimulationModel").update()
    container.get_entity_by_name("Simulation").update()


def test_inheritance_in_same_container():
    """This test covers three cases:

        1. inheritance=db.SUGGESTED
        2. inheritance=db.RECOMMENDED
        3. inheritance=db.OBLIGATORY
    """
    p = db.Property("TestProperty1", datatype=db.TEXT)
    rt1 = db.RecordType("TestRT1").add_property(p, importance=db.RECOMMENDED)
    rt2 = db.RecordType("TestRT2").add_parent("TestRT1",
                                              inheritance=db.SUGGESTED)
    rt3 = db.RecordType("TestRT3").add_parent("TestRT1",
                                              inheritance=db.RECOMMENDED)
    rt4 = db.RecordType("TestRT4").add_parent("TestRT1",
                                              inheritance=db.OBLIGATORY)
    c = db.Container().extend([p, rt1, rt2, rt3, rt4])

    c.insert()

    assert len(rt1.get_properties()) == 1
    assert len(rt2.get_properties()) == 1
    assert len(rt3.get_properties()) == 1
    assert len(rt4.get_properties()) == 0
