# encoding: utf-8
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
"""Created on 22.01.2017.

@author: tf
"""
import caosdb as db
from pytest import raises


def setup_function(function):
    try:
        db.execute_query("FIND ENTITY").delete()
    except BaseException:
        pass


def teardown_function(function):
    setup_function(function)


def test_override_with_non_existing_ref():
    rt1 = db.RecordType("TestRecordType1").insert()
    rt2 = db.RecordType("TestRecordType2").insert()
    db.Property("TestProperty", datatype=db.TEXT).insert()

    with raises(db.TransactionError) as cm:
        db.Record(
            name="TestRecord").add_property(
            name="TestProperty",
            datatype=rt2,
            value="Non-Existing").add_parent(rt1).insert()
    assert cm.value.has_error(db.UnqualifiedPropertiesError)
    assert (cm.value.errors[0].errors[0].msg ==
            "Referenced entity does not exist.")


def test_override_with_existing_ref():
    rt1 = db.RecordType("TestRecordType1").insert()
    rt2 = db.RecordType("TestRecordType2").insert()
    db.Property("TestProperty", datatype=db.TEXT).insert()
    db.Record("TestRecord").add_parent(name="TestRecordType2").insert()

    rec2 = db.Record(
        name="TestRecord2").add_property(
        name="TestProperty",
        datatype=rt2,
        value="TestRecord").add_parent(rt1).insert()

    assert rec2.is_valid() is True


def test_reference_datatype_sequencial():
    rt = db.RecordType(name="TestRT", description="TestRTDesc").insert()
    p = db.Property(
        name="TestProp",
        description="RefOnTestRT",
        datatype="TestRT").insert()

    assert p.is_valid() is True

    dt = db.execute_query("FIND ENTITY TestProp", unique=True).datatype
    assert dt == "TestRT"

    rt2 = db.RecordType(
        "TestRT2",
        description="TestRT2Desc").add_property(
        name="TestProp",
        value="TestRT").insert()
    assert rt2.is_valid() is True
    assert rt2.get_property("TestProp").value == rt.id
    assert rt2.get_property("TestProp").datatype == "TestRT"


def test_reference_datatype_at_once():
    rt = db.RecordType(name="TestRT")
    rt2 = db.RecordType(name="TestRT2").add_property(name="TestProp")
    rec = db.Record().add_parent(name="TestRT")
    rec2 = db.Record().add_parent(
        name="TestRT2").add_property(
        name="TestProp",
        value=rec)
    p = db.Property(name="TestProp", datatype="TestRT")

    c = db.Container().extend([rt, rt2, rec, rec2, p]).insert()
    assert c.is_valid() is True


def test_generic_reference_success():
    rt1 = db.RecordType(name="TestRT1").insert()
    rt2 = db.RecordType(name="TestRT2").insert()
    p = db.Property(name="TestP1", datatype=db.REFERENCE).insert()
    rec1 = db.Record(name="TestRec1").add_parent(name="TestRT1").insert()
    rec2 = db.Record(
        name="TestRec2").add_parent(
        name="TestRT2").add_property(
            name="TestP1",
        value=rec1.id).insert()

    assert rt1.is_valid() is True
    assert rt2.is_valid() is True
    assert p.is_valid() is True
    assert rec1.is_valid() is True
    assert rec2.is_valid() is True


def test_generic_reference_failure():
    db.RecordType(name="TestRT2").insert()
    db.Property(name="TestP1", datatype=db.REFERENCE).insert()
    rec2 = db.Record(
        name="TestRec2").add_parent(
        name="TestRT2").add_property(
            name="TestP1",
        value="asdf")
    raises(db.TransactionError, rec2.insert)


def test_unknown_datatype1():
    p = db.Property(name="TestP", datatype="Non-Existing")
    with raises(db.TransactionError) as te:
        p.insert()
    assert te.value.errors[0].msg == "Unknown data type."


def test_unknown_datatype2():
    p = db.Property(name="TestP", datatype="12345687654334567")
    with raises(db.TransactionError) as te:
        p.insert()
    assert te.value.errors[0].msg == "Unknown data type."


def test_unknown_datatype3():
    p = db.Property(name="TestP", datatype="-134")
    with raises(db.TransactionError) as te:
        p.insert()
    assert te.value.errors[0].msg == "Unknown data type."


def test_wrong_refid():
    rt1 = db.RecordType(name="TestRT1").insert()
    rt2 = db.RecordType(name="TestRT2").insert()
    rt3 = db.RecordType(name="TestRT3").insert()
    p = db.Property(name="TestP1", datatype=rt1.id).insert()
    assert p.is_valid()
    assert rt1.is_valid()
    assert rt2.is_valid()
    assert rt3.is_valid()

    rec1 = db.Record().add_parent(name="TestRT1").insert()
    rec2 = db.Record().add_parent(name="TestRT2").insert()
    rec3 = db.Record().add_parent(
        name="TestRT3").add_property(
        name="TestP1",
        value=rec2.id)
    with raises(db.TransactionError):
        rec3.insert()
    desc = ('Reference not qualified. The value of this Reference '
            'Property is to be a child of its data type.')
    err = rec3.get_property("TestP1").get_errors()[0]
    assert err.description == desc

    rec4 = db.Record().add_parent(
        name="TestRT3").add_property(
        name="TestP1",
        value=rec1.id).insert()
    assert rec4.is_valid()


def test_datatype_mismatch_in_response():
    p = db.Property("TestDoubleProperty", datatype=db.DOUBLE).insert()

    with raises(ValueError) as exc:
        rt = db.RecordType().add_property(p, "not a double")
        assert exc.value.args[0] == ("could not convert string to "
                                     "float: 'not a double'")

    # add property by name,
    rt = db.RecordType("TestEntity").add_property(p.name, "not a double")

    with raises(db.TransactionError) as exc:
        # should not raise ValueError but transaction error.
        rt.insert()
    assert exc.value.errors[0].errors[0].msg == "Cannot parse value to double."
