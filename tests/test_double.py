# encoding: utf-8
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2019 IndiScale GmbH
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#

import math
import caosdb as db


def setup_module():
    clean = db.execute_query("FIND ENTITY")
    if len(clean) > 0:
        clean.delete()


def teardown_module():
    setup_module()


def test_nan():
    p = db.Property(name="TestDoubleProperty", datatype=db.DOUBLE).insert()
    rt = db.RecordType(name="TestRecordType").add_property(name="TestDoubleProperty",
                                                           value="NaN").insert()

    test1 = db.execute_query("FIND ENTITY TestRecordType", unique=True)
    assert math.isnan(test1.get_property("TestDoubleProperty").value)

    test2 = db.execute_query(
        "FIND ENTITY TestRecordType WITH TestDoubleProperty = NaN", unique=True)
    assert math.isnan(test1.get_property("TestDoubleProperty").value)
