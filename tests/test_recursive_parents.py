# encoding: utf-8
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
# Copyright (C) 2020 IndiScale GmbH <info@inidscale.com>
# Copyright (C) 2020 Florian Spreckelsen <f.spreckelsen@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
"""Created on 2016-01-14.

Test if recursive parents are found correctly.

"""
import caosdb as db
import pytest


def setup_module():
    """Delete all test entities"""
    db.execute_query("FIND ENTITY Test*").delete(
        raise_exception_on_error=False)


def setup_function(function):
    """No further setup required"""
    setup_module()


def teardown_function(function):
    """Delete again"""
    setup_module()


def test_get_parents_recursively():
    """Test for https://gitlab.com/caosdb/caosdb-pylib/-/issues/90

> Entity.get_parents_recursively() does not work unless the full ancestry has been retrieved from
> the server.
"""
    # Setup
    rt_A = db.RecordType(name="TestA")
    rt_B = db.RecordType(name="TestB").add_parent(rt_A)
    rt_C = db.RecordType(name="TestC").add_parent(rt_B)
    db.Container().extend([rt_A, rt_B, rt_C]).insert()

    # Retrieve only C
    retrieved_C = db.RecordType(name="TestC").retrieve()
    parents = retrieved_C.get_parents_recursively(retrieve=True)
    assert len(parents) == 2
    assert "TestB" in [p.name for p in parents]
    assert "TestA" in [p.name for p in parents]


def test_recursive_parents():
    # inheritance structure:
    #    A
    #   / \
    #  B  B2
    #   \ /
    #    C
    #    |
    #    c
    A = db.RecordType(name="TestTypeA").insert()
    B = db.RecordType(name="TestTypeB").add_parent(A).insert()
    B2 = db.RecordType(name="TestTypeB2").add_parent(A).insert()
    C = db.RecordType(name="TestTypeC").add_parent(B).add_parent(B2).insert()
    c = db.Record(name="TestRecord").add_parent(C).insert()

    parents = C.get_parents_recursively()
    assert len(parents) == 3
    parent_identifiers = [(all_p.id, all_p.name) for all_p in parents]
    assert (A.id, A.name) in parent_identifiers
    assert (B.id, B.name) in parent_identifiers
    assert (B2.id, B2.name) in parent_identifiers

    parents = c.get_parents_recursively()
    assert len(parents) == 4
    parent_identifiers = [(all_p.id, all_p.name) for all_p in parents]
    assert (A.id, A.name) in parent_identifiers
    assert (B.id, B.name) in parent_identifiers
    assert (B2.id, B2.name) in parent_identifiers
    assert (C.id, C.name) in parent_identifiers

    # Now do a time travel and great-grand-parentize yourself...
    A.add_parent(C).update()

    parents = C.get_parents_recursively()
    assert len(parents) == 4
    parent_identifiers = [(all_p.id, all_p.name) for all_p in parents]
    assert (A.id, A.name) in parent_identifiers
    assert (B.id, B.name) in parent_identifiers
    assert (B2.id, B2.name) in parent_identifiers
    assert (C.id, C.name) in parent_identifiers


def test_entity_has_parent():
    # inheritance structure:
    #    A
    #   / \
    #  B  B2
    #   \ /
    #    C
    #    |
    #    c
    A = db.RecordType(name="TestTypeA").insert()
    B = db.RecordType(name="TestTypeB").add_parent(A).insert()
    B2 = db.RecordType(name="TestTypeB2").add_parent(A).insert()
    C = db.RecordType(name="TestTypeC").add_parent(B).add_parent(B2).insert()
    c = db.Record(name="TestRecord").add_parent(C).insert()

    assert C.has_parent(B)
    assert c.has_parent(B)
    assert c.has_parent(A)

    assert not C.has_parent(C)
    assert not A.has_parent(C)
    assert not B.has_parent(C)

    # Now do a time travel and great-grand-parentize yourself...
    A.add_parent(C).update()

    assert C.has_parent(C)

    # Non-recursive tests
    assert C.has_parent(B, recursive=False)
    assert not c.has_parent(B, recursive=False)
    assert not c.has_parent(A, recursive=False)
    assert not C.has_parent(C, recursive=False)

    # Works by name or ID
    fake_B_name = db.RecordType(name="TestTypeB")
    fake_C_name = db.RecordType(name="not C")

    assert c.has_parent(fake_B_name, check_name=True)
    assert not c.has_parent(fake_C_name, check_name=True)

    fake_B_id = db.RecordType(id=B.id)
    fake_C_id = db.RecordType(id=C.id * 5)

    assert c.has_parent(fake_B_id, check_name=False, check_id=True)
    assert not c.has_parent(fake_C_id, check_name=False,
                            check_id=True)

    fake_B_name_id = db.RecordType(name="TestTypeB", id=B.id)
    fake_C_name_id = db.RecordType(name="not C", id=C.id * 5)

    assert c.has_parent(fake_B_name_id, check_name=True,
                        check_id=True)
    assert not c.has_parent(fake_C_name_id, check_name=True,
                            check_id=True)
