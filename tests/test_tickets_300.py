# -*- coding: utf-8 -*-
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
# Copyright (C) 2020 Indiscale GmbH <info@indiscale.com>
# Copyright (C) 2020 Florian Spreckelsen <f.spreckelsen@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
"""Tests for gitlab issues from #300-#399."""

from __future__ import unicode_literals, print_function
import caosdb as db
from pytest import raises


def _delete_test_entities():
    try:
        db.execute_query("FIND Test*").delete()
    except Exception as e:
        print(e)


def setup_module():
    _delete_test_entities()


def teardown_module():
    _delete_test_entities()


def setup_function(function):
    setup_module()


def teardown_function(function):
    teardown_module()


def test_ticket_350_insert_with_invalid_ref():
    c = db.Container()
    e = db.Entity(name="TestEnt")
    rt1 = db.RecordType(name="TestRT1")
    rt2 = db.RecordType(name="TestRT2").add_property(
        name="TestRT1", value="TestEnt")
    c.extend([e, rt1, rt2])
    with raises(db.TransactionError) as cm:
        c.insert()
    assert any(
        [x.msg == "There is no such role 'Entity'." for x in cm.value.errors])
    assert cm.value.has_error(db.UnqualifiedPropertiesError)
