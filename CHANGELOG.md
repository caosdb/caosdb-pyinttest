# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added (for new features)

* Tests for [linkahead-pylib#127](https://gitlab.com/linkahead/linkahead-pylib/-/issues/127)
* Tests for [linkahead-server#280](https://gitlab.com/linkahead/linkahead-server/-/issues/280)
* Test for [caosdb-pylib#119](https://gitlab.com/linkahead/linkahead-pylib/-/issues/119)
* Test for [caosdb-pylib#89](https://gitlab.com/linkahead/linkahead-pylib/-/issues/89)
* Test for [caosdb-pylib#103](https://gitlab.com/linkahead/linkahead-pylib/-/issues/103)
* Tests for entity state [caosdb-server!62](https://gitlab.com/caosdb/caosdb-server/-/merge_requests/62)
* Tests for version history
* Tests for inheritance bug (caosdb/caosdb-server!54)
* Tests for versioning
* Tests for deeply nested SELECT queries
* Tests for [#62](https://gitlab.com/caosdb/caosdb-server/-/issues/62)
* Tests for One-time Authentication Tokens
* Test for [caosdb-pylib#31](https://gitlab.com/caosdb/caosdb-pylib/-/issues/31)
* Tests for [caosdb-server#62](https://gitlab.com/caosdb/caosdb-server/-/issues/62)
  in caosdb-server-project, i.e., renaming of a RecordType that should
  be reflected in properties with that RT as datatype.
* Test for [#39](https://gitlab.com/caosdb/caosdb-server/-/issues/39)
  in caosdb server which checks if quoted datetimes in queries are
  evaluated correctly.
* Test for [caosdb-server#99](https://gitlab.com/caosdb/caosdb-server/-/issues/99).
* Tests for retrieving older versions.
* Tests for `BEFORE`, `AFTER`, `UNTIL`, `SINCE` keywords for query
  transaction filters
  [caosdb-server#132](https://gitlab.indiscale.com/caosdb/src/caosdb-server/-/issues/132)
* Tests for missing obligatory Properties
  [caosdb-server#146](https://gitlab.indiscale.com/caosdb/src/caosdb-server/-/issues/146)
* Tests for updates of SELECT results, two still marked as `xfail`
  until
  [caosdb-pylib#48](https://gitlab.indiscale.com/caosdb/src/caosdb-pylib/-/issues/48)
  and
  [caosdb-server#155](https://gitlab.indiscale.com/caosdb/src/caosdb-server/-/issues/155)
  have been resolved.
* Tests for caosdb-server#154
* Tests for caosdb-server#217
* Tests for caosdb-pylib#61
* Test for [caosdb-server#136](https://gitlab.com/caosdb/caosdb-server/-/issues/136)
* Test for [caosdb-server#141](https://gitlab.com/caosdb/caosdb-server/-/issues/141)
* Test for [caosdb-server#145](https://gitlab.com/caosdb/caosdb-server/-/issues/145)
* Tests for [caosdb-pylib#90](https://gitlab.com/caosdb/caosdb-pylib/-/issues/90): `Entity.get_parents_recursively()` did not work for unretrieved
  parents.
* Test for [caosdb-server#192](https://gitlab.com/caosdb/caosdb-server/-/issues/192)
* Test for miscellaneous *-too-long errors.
* Test for [caosdb-server#263](https://gitlab.com/linkahead/linkahead-server/-/issues/263): Unexpected server error when attempting to retrieve internal ROLE
  entities.
* Test for [caosdb-server#268](https://gitlab.com/linkahead/linkahead-server/-/issues/268): Unexpected Server Error when non-existent file shall be inserted.
* test_profile for running the tests locally.

### Changed (for changes in existing functionality)

* `setup.py` has been replaced by a `requirements.txt`.
* Tests comply with the new entity error handling (see
  [#32](https://gitlab.com/caosdb/caosdb-pylib/-/issues/32) in
  caosdb-pylib).
* `test_recursive_parents.py` now tests inserted entities; set to
  xfail until
  [caosdb-pylib#34](https://gitlab.com/caosdb/caosdb-pylib/-/issues/34)
  is fixed.
* Test for [caosdb-server#197](https://gitlab.com/caosdb/caosdb-server/-/issues/197).

### Deprecated (for soon-to-be removed features)

### Removed (for now removed features)

- Support for tox
- Some redundant tests from `test_tickets.py` that checked
  functionality that was already tested in `test_error_stuff.py`.
- `test_server_side_scripting.bin_dir.local` option in pylinkahead.ini

### Fixed (for any bug fixes)

* Tests for NaN Double Values (see https://gitlab.com/caosdb/caosdb-server/issues/41)
* Tests for name queries. [caosdb-server#51](https://gitlab.com/caosdb/caosdb-server/-/issues/51)
* Server-side scripting is more tolerant to Pandas warnings now. (https://gitlab.indiscale.com/caosdb/src/caosdb-pyinttest/-/issues/21)

### Security (in case of vulnerabilities)
