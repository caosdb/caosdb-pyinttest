# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2020 IndiScale GmbH <info@indiscale.com>
# Copyright (C) 2020 Daniel Hornung <d.hornung@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header

# Just a collection of scripts

.PHONY: all autopep8 test help

# Do the integration tests.
# Accepts freestyle arguments like so:
# `make test test_opts=test_foo.py -k test_function`
test:
	cd tests &&	pytest $(test_opts)


# Just print the help.
define HELP
The most common targets:

make test :: Do the integration tests.  Accepts freestyle arguments like so:
             `make test test_opts=test_foo.py -k test_function`
Prepare everything for running.
make autopep8 :: Autoformatting for all tests
make all :: Run autopep8 and test
endef
export HELP
help:
	@echo "$$HELP"


# Run the tests through autopep8.
autopep8:
	autopep8 -ri .

# Meta target to call the other targets.
all: autopep8 test


mypy:
	mypy tests
